const createError = require('http-errors');
const express = require('express');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const CronJob = require('cron').CronJob;
// const exec = require('child_process').exec;

const dataTask = require('./data-retriever');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));

app.use('/', indexRouter);

app.use(function (req, res, next) {
  next(createError(404));
});

app.use(function (err, req, res) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});

app.listen(8081, () => {
  console.log('listening on 8081');
});

new CronJob('0 */10 * * * *', async () => {
  await dataTask.startDataTask();
}).start();

module.exports = app;
