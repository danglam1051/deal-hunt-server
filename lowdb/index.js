const low = require('lowdb');
const path = require('path');
const FileSync = require('lowdb/adapters/FileSync');
const {LOWDB_DB_NAME} = require('../util/constants');

module.exports.useDb = (dbName) => {
  let dbFile, dbDefaults;

  switch (dbName) {
    case LOWDB_DB_NAME.LAZADA:
      dbFile = (path.resolve(`${__dirname}/product-lazada.json`));
      break;
    case LOWDB_DB_NAME.SHOPEE:
      dbFile = (path.resolve(`${__dirname}/product-shopee.json`));
      break;
    default:
      throw new Error('Invalid db name');
  }

  const adapter = new FileSync(dbFile);
  const db = low(adapter);
  db.defaults({products: []}).write();

  return db;
};
