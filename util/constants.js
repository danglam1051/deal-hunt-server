module.exports.END_POINTS = Object.freeze({
    LAZADA_FLASH_SALE: 'https://www.lazada.vn/pages/i/vn/act/lp-flash-sale',
    SHOPEE_FLASH_SALE: 'https://shopee.vn/flash_sale/',
    SENDO_FLASH_SALE: 'https://api.sendo.vn/flash-deal/ajax-product/',
});

module.exports.LOWDB_DB_NAME = Object.freeze({
    LAZADA: 'lazada',
    SHOPEE: 'shopee',
});
