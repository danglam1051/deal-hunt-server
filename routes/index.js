const express = require('express');
const router = express.Router();

const {useDb} = require('../lowdb');
const {LOWDB_DB_NAME} = require('../util/constants');

let lowdb;

router.post('/lazada', (req, res) => {
  const key = req.body.key;

  if (key === "atSE>x+FmYS%>M&RA/[])ns85T?>,b`enZK!bgg%WD'Jf:;YE';_#>=g#E3evg/F") {
    lowdb = useDb(LOWDB_DB_NAME.LAZADA);
    const data = lowdb.get('products').value();
    res.send(200, data);
  } else {
    res.send(500, 'unauthorized');
  }
});

router.post('/shopee', (req, res) => {
  const key = req.body.key;

  if (key === "atSE>x+FmYS%>M&RA/[])ns85T?>,b`enZK!bgg%WD'Jf:;YE';_#>=g#E3evg/F") {
    lowdb = useDb(LOWDB_DB_NAME.SHOPEE);
    const data = lowdb.get('products').value();
    res.send(200, data);
  } else {
    res.send(500, 'unauthorized');
  }
});

router.post('/sendo', (req, res) => {
  const key = req.body.key;

  if (key === "atSE>x+FmYS%>M&RA/[])ns85T?>,b`enZK!bgg%WD'Jf:;YE';_#>=g#E3evg/F") {
    const data = fs.readFileSync('data-sendo.txt', 'utf8');
    res.send(200, data);
  } else {
    res.send(500, 'unauthorized');
  }
});

router.post('/all', (req, res) => {
  const key = req.body.key;

  if (key === "atSE>x+FmYS%>M&RA/[])ns85T?>,b`enZK!bgg%WD'Jf:;YE';_#>=g#E3evg/F") {
    lowdb = useDb(LOWDB_DB_NAME.LAZADA);
    const dataLazada = lowdb.get('products').value();
    lowdb = useDb(LOWDB_DB_NAME.SHOPEE);
    const dataShopee = lowdb.get('products').value();
    // const dataSendo = JSON.parse(fs.readFileSync('./data-retriever/data-sendo.txt', 'utf8'));
    // const data = dataLazada.concat(dataShopee).concat(dataSendo);
    const data = dataLazada.concat(dataShopee);
    res.send(200, data);
  } else {
    res.send(500, 'unauthorized');
  }
});

module.exports = router;
