const axios = require('axios');
const {END_POINTS} = require('../util/constants');

const getProductList = async () => {
  const requestBody = {
    "page": 1,
    "limit": 10000,
    "special_status": 0,
    "category_group_id": 0,
    "buy_limit": 0,
    "shoptype": 0,
    "is_new_app": 0,
    "tag": 0
  };

  try {
    const list = await axios.post(END_POINTS.SENDO_FLASH_SALE, requestBody);
    console.log(list);
  } catch (e) {
    console.error(e);
    return null;
  }

  // const resultString = await request(options);
  // console.log(resultString);
  // const result = JSON.parse(resultString);
  //
  // const productList = [];
  //
  // result.data.products.forEach((item) => {
  //   const product = new Product(item.url_key.trim(), item.image.trim(), item.name.trim(), item.final_price, item.price, Math.round((item.price - item.final_price) / item.price * 100) + '%', 'sendo');
  //
  //   productList.push(product);
  // });
  //
  // fs.writeFile('data-sendo.txt', JSON.stringify(productList), function (err) {
  //   if (err)
  //     return console.log(err);
  //   console.log('Sendo data written');
  // });

  // return productList;
};

getProductList();
