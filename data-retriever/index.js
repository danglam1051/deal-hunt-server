const Puppeteer = require('puppeteer');
const {useDb} = require('../lowdb');
const {LOWDB_DB_NAME} = require('../util/constants');
const shell = require('shelljs');

const lazadaCrawler = require('./lazada');
const shopeeCrawler = require('./shopee');
// const sendo = require('./data-sendo');

module.exports.startDataTask = async () => {
  let browser;

  try {
    browser = await Puppeteer.launch({
      headless: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage']
    });

    const shopeePage = await browser.newPage();

    const shopeeProductList = await shopeeCrawler.getProductList(shopeePage);
    let lowDb = useDb(LOWDB_DB_NAME.SHOPEE);
    lowDb.set('products', []).write();
    shopeeProductList.forEach(product => lowDb.get('products').push(product).write());
    await shopeePage.close();

    console.log(shopeeProductList.length);

    const lazadaPage = await browser.newPage();

    const lazadaProductList = await lazadaCrawler.getProductList(lazadaPage);
    lowDb = useDb(LOWDB_DB_NAME.LAZADA);
    lowDb.set('products', []).write();
    lazadaProductList.forEach(product => lowDb.get('products').push(product).write());
    await lazadaPage.close();

    console.log('all done!');
  } catch (e) {
    console.error(e);
  } finally {
    if (browser) await browser.close();
    setTimeout(() => shell.exec('pkill chrome'), 30 * 1000);
  }
};
