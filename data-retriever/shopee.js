const cheerio = require('cheerio');
const {END_POINTS} = require('../util/constants');

const getScrollHeight = async (page) => page.evaluate('document.documentElement.scrollTop');

const scrollPage = async (page, scrollHeight, scrollHeightIncrement, delayBetweenScrolls, heightCheckLimit) => {
  let heightCheckCount = 0;
  let previousHeight, newHeight;

  while (true) {
    if (heightCheckCount === heightCheckLimit) break;

    previousHeight = await getScrollHeight(page);
    await page.evaluate(`window.scrollTo(0, ${scrollHeight})`);
    scrollHeight += scrollHeightIncrement;
    await page.waitFor(delayBetweenScrolls);
    newHeight = await getScrollHeight(page);

    if (previousHeight === newHeight) heightCheckCount++;
    else heightCheckCount = 0;
  }

  return null;
};

module.exports.getProductList = async (page) => {
  await page.setViewport({width: 1920, height: 1080});
  await page.goto(END_POINTS.SHOPEE_FLASH_SALE, {
    waitUntil: 'networkidle2',
    timeout: 0
  });

  // first scroll is to load all products
  await scrollPage(page, 1000000, 0, 2500, 3);
  // second scroll starts from beginning to load all lazy-loaded images
  await scrollPage(page, 0, 175, 200, 3);

  const pageHtml = await page.evaluate(() => document.body.innerHTML);
  let $ = cheerio.load(pageHtml);

  const saleItems = $('div.flash-sale-items > div.flash-sale-item-card:not(.flash-sale-item-card--sold-out)');
  const productList = [];

  saleItems.each((index) => {
    const item = cheerio.load(saleItems.eq(index).html());

    const hrefEl = item('a.flash-sale-item-card-link');
    const imgEl = item('a.flash-sale-item-card-link div.flash-sale-item-card__animated-image');
    const titleEl = item('a.flash-sale-item-card-link div.flash-sale-item-card__item-name');
    const originalPriceEl = item('a.flash-sale-item-card-link div.flash-sale-item-card__original-price > span.item-price-number');
    const salePriceEl = item('a.flash-sale-item-card-link div.flash-sale-item-card__current-price > span.item-price-number');
    const discountEl = item('a.flash-sale-item-card-link div.shopee-badge--promotion__label-wrapper > span.percent');

    const link = `https://shopee.vn${hrefEl.attr('href')}`.trim();
    const imgSrc = imgEl.attr('style').match(/"((?:\\.|[^"\\])*)"/)[0].replace(/"/g, "").trim();
    const title = titleEl.text().trim();
    const originalPrice = originalPriceEl.text().replace(/\./g, '').trim();
    const salePrice = salePriceEl.text().replace(/\./g, '').trim();
    const discount = discountEl.text().split('.')[0].trim();

    productList.push({
      link,
      imgSrc,
      title,
      salePrice,
      originalPrice,
      discount,
      from: 'shopee'
    });
  });

  return productList;
};
