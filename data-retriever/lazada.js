const cheerio = require('cheerio');
const {END_POINTS} = require('../util/constants');

module.exports.getProductList = async (page) => {
  await page.goto(END_POINTS.LAZADA_FLASH_SALE, {
    waitUntil: 'networkidle2'
  });

  const pageHtml = await page.evaluate(() => document.body.innerHTML);
  let $ = cheerio.load(pageHtml);

  const saleItems = $(`div.item-list.J_FSItemList:first-child > div.item-list-content a`);
  const productList = [];

  saleItems.each((index) => {
    const item = cheerio.load(saleItems.eq(index).html());

    const imgEl = item('div.flash-unit-image img');
    const titleEl = item('div.unit-content div.sale-title');
    const salePriceEl = item('div.unit-content div.sale-price');
    const originalPriceEl = item('div.unit-content div.origin-price span.origin-price-value');
    const discountEl = item('div.unit-content div.origin-price span.discount');

    const link = saleItems.eq(index).attr('href').trim();
    const imgSrc = imgEl.attr('src') ? imgEl.attr('src').trim() : imgEl.attr('data-ks-lazyload').trim();
    const title = titleEl.text().trim();
    const salePrice = salePriceEl.text().replace(/[ ₫.,]/g, '').trim();
    const originalPrice = originalPriceEl.text().replace(/[ ₫.,]/g, '').trim();
    const discount = discountEl.text().split('.')[0].replace(/-/g, '').trim();

    productList.push({
      link,
      imgSrc,
      title,
      salePrice,
      originalPrice,
      discount,
      from: 'lazada'
    });
  });

  return productList;
};
